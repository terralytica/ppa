# Terralytica PPA

## Ubuntu Focal

### Install Repo

```sh
curl -s --compressed "https://apt.terralytica.io/KEY.gpg" | sudo apt-key add -

sudo curl -s --compressed -o /etc/apt/sources.list.d/terralytica.list "https://apt.terralytica.io/ubuntu/focal/terralytica.io.list"

sudo apt update

sudo apt install kdms provctl
```
